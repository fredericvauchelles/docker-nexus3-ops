#!/bin/bash

DATA_DIR=${DATA_DIR-/nexus-data}

case "$1" in
    package)
  [ -z "$2" ] && echo "Missing output file argument" && exit 1;
  [ -e "$2" ] && [ ! -w "$2" ] && echo "Target file is not writeable" && exit 1;

  cd ${DATA_DIR}

  # compress
  tar czf "$2" *
  ;;
    restore)
  [ -z "$2" ] && echo "Missing output file argument" && exit 1;
  
  tar -x -C ${DATA_DIR} -f "$2"
  ;;
    *)
  exec "$@"
  ;;
esac